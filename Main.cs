﻿using BepInEx;
using BepInEx.Configuration;
using GameplayEntities;
using LLBML.Utils;
using LLHandlers;
using LLScreen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InvisBallSimple
{
    [BepInPlugin("com.gitlab.axolotlll.invisballsimple", "InvisBall", "1.0.0")]
    public class Main : BaseUnityPlugin
    {
        public bool InGame => World.instance != null && !UIScreen.loadingScreenActive && (DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.CDOFDJMLGLO || DNPFJHMAIBP.HHMOGKIMBNM() == JOFJHDJHJGI.LGILIJKMKOD);
        public ConfigEntry<bool> doInvisBall;
        void Awake()
        {
            doInvisBall = Config.Bind<bool>("Toggles", "invisBall", true, "Ball is invisible");
        }
        void Start()
        {
            ModDependenciesUtils.RegisterToModMenu(this.Info);
        }

        void Update()
        {

            if (InGame && doInvisBall.Value){
                BallEntity ent = BallHandler.instance.GetBall(0);
                if (ent.hitableData.hitstunState == HitstunState.HIT_PLAYER_STUN)
                {
                    ent.SetVisible(true, "main");
                    ent.SetBallTrailActive(true);
                }
                else
                {
                    ent.PlayAnim("off", "glowVisual");
                    ent.SetBallTrailActive(false);
                    ent.SetVisible(false, "main");
                }
                
            }
           
        }
    }
}
